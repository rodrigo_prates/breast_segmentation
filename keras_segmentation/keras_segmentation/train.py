import json
from .data_utils.data_loader import image_segmentation_generator, \
    verify_segmentation_dataset
import glob
import six
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, CSVLogger, LearningRateScheduler, ReduceLROnPlateau
import os
from keras import backend as K
from keras.optimizers import Adam, Adadelta
from keras.metrics import MeanIoU
import tensorflow as tf


def find_latest_checkpoint(checkpoints_path, fail_safe=True):

    def get_epoch_number_from_path(path):
        return path.replace(checkpoints_path, "").strip(".")

    # Get all matching files
    all_checkpoint_files = glob.glob(checkpoints_path + ".*")
    # Filter out entries where the epoc_number part is pure number
    all_checkpoint_files = list(filter(lambda f: get_epoch_number_from_path(f)
                                       .isdigit(), all_checkpoint_files))
    if not len(all_checkpoint_files):
        # The glob list is empty, don't have a checkpoints_path
        if not fail_safe:
            raise ValueError("Checkpoint path {0} invalid"
                             .format(checkpoints_path))
        else:
            return None

    # Find the checkpoint file with the maximum epoch
    latest_epoch_checkpoint = max(all_checkpoint_files,
                                  key=lambda f:
                                  int(get_epoch_number_from_path(f)))
    return latest_epoch_checkpoint

def find_latest_checkpoint_by_date( checkpoints_path, checkpoints_file_path=None ):
    if checkpoints_file_path:
        return checkpoints_file_path
    modelfolderpath = os.path.dirname(checkpoints_path)
    filenames = sorted(glob.iglob(os.path.sep.join([modelfolderpath, '*'])), key=os.path.getctime, reverse=True)
    newestfile = filenames[0]
    if newestfile.split('.')[-1] == 'json' or newestfile.split('.')[-1] == 'csv':
        newestfile = newestfile.split('.')[0]
    #modelfilecomponents = newestfile.split('.')
    #if modelfilecomponents[0] == checkpoints_path and modelfilecomponents[1].isnumeric() and os.path.isfile(newestfile):
    if newestfile == checkpoints_path and os.path.isfile(newestfile):
        return checkpoints_path
    else:
        return find_latest_checkpoint( checkpoints_path )

def masked_categorical_crossentropy(gt, pr):
    from keras.losses import categorical_crossentropy
    mask = 1 - gt[:, :, 0]
    return categorical_crossentropy(gt, pr) * mask

def jaccard_distance(y_true, y_pred, smooth=100):
    intersection = K.sum(K.abs(y_true * y_pred), axis=-1)
    sum_ = K.sum(K.abs(y_true) + K.abs(y_pred), axis=-1)
    jac = (intersection + smooth) / (sum_ - intersection + smooth)
    return (1 - jac) * smooth

def iou_coef(y_true, y_pred, smooth=1):
    intersection = K.sum(K.abs(y_true * y_pred), axis=[1,2,3])
    union = K.sum(y_true,[1,2,3])+K.sum(y_pred,[1,2,3])-intersection
    iou = K.mean((intersection + smooth) / (union + smooth), axis=0)
    return iou

#def dice_coef(y_true, y_pred, smooth=1e-6):
#    intersection = K.sum(y_true * y_pred, axis=[1,2,3])
#    union = K.sum(y_true, axis=[1,2,3]) + K.sum(y_pred, axis=[1,2,3])
#    dice = K.mean((2. * intersection + smooth)/(union + smooth), axis=0)
#    return dice

#def dice_coef(y_true, y_pred, smooth=1e-6):
#    y_true_f = K.flatten(y_true)
#    y_pred_f = K.flatten(y_pred)
#    intersection = K.sum(y_true_f * y_pred_f)
#    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

#def dice_coef_multilabel(y_true, y_pred, numLabels=3):
#    dice=0
#    for index in range(numLabels):
#        dice -= dice_coef(y_true[:,:,:,index], y_pred[:,:,:,index])
#    return dice

#def dice_coef(y_true, y_pred, smooth=1e-7):
    '''
    Dice coefficient for 3
     categories. Ignores background pixel label 0
    Pass to model as metric during compile statement
    '''
#    y_true_f = K.flatten(K.one_hot(K.cast(y_true, 'int32'), num_classes=3))
#    y_pred_f = K.flatten(y_pred)
#    intersect = K.sum(y_true_f * y_pred_f, axis=-1)
#    denom = K.sum(y_true_f + y_pred_f, axis=-1)
#    return K.mean((2. * intersect / (denom + smooth)))

#def dice_coef_multilabel(y_true, y_pred):
    '''
    Dice loss to minimize. Pass to model as loss during compile statement
    '''
#    return 1 - dice_coef(y_true, y_pred)

#def generalized_dice_coeff(y_true, y_pred):
#    n_el = 1
#    for dim in y_train.shape: 
#        n_el *= int(dim)
#    n_cl = y_train.shape[-1]
#    w = K.zeros(shape=(n_cl,))
#    w = (K.sum(y_true, axis=(0,1,2)))/(n_el)
#    w = 1/(w**2+0.000001)
#    numerator = y_true*y_pred
#    numerator = w*K.sum(numerator,(0,1,2))
#    numerator = K.sum(numerator)
#    denominator = y_true+y_pred
#    denominator = w*K.sum(denominator,(0,1,2))
#    denominator = K.sum(denominator)
#    return 2*numerator/denominator

#def generalized_dice_coeff(y_true, y_pred):
#    Ncl = 3
#    w = K.zeros(shape=(Ncl,))
#    w = K.sum(y_true, axis=(0,1,2))
#    w = 1/(w**2+0.000001)
    # Compute gen dice coef:
#    numerator = y_true*y_pred
#    numerator = w*K.sum(numerator,(0,1,2,3))
#    numerator = K.sum(numerator)

#    denominator = y_true+y_pred
#    denominator = w*K.sum(denominator,(0,1,2,3))
#    denominator = K.sum(denominator)

#    gen_dice_coef = 2*numerator/denominator

#    return gen_dice_coef

#def dice_coef_multilabel(y_true, y_pred):
#    return 1 - generalized_dice_coeff(y_true, y_pred)

def dice_coef_multilabel(y_true, y_pred):
    alpha = 0.5
    beta  = 0.5
    
    ones = K.ones(K.shape(y_true))
    p0 = y_pred      # proba that voxels are class i
    p1 = ones-y_pred # proba that voxels are not class i
    g0 = y_true
    g1 = ones-y_true
    
    num = K.sum(p0*g0, (0,1,2))
    den = num + alpha*K.sum(p0*g1,(0,1,2)) + beta*K.sum(p1*g0,(0,1,2))
    
    T = K.sum(num/den) # when summing over classes, T has dynamic range [0 Ncl]
    
    #Ncl = 3.0
    #tf.keras.backend.print_tensor(y_true)
    return 1-T

#https://gist.github.com/ilmonteux/8340df952722f3a1030a7d937e701b5a
def seg_metrics(y_true, y_pred, metric_name, metric_type='standard', drop_last = False, mean_per_class=False, num_classes=3, verbose=False):

    """ 
    Compute mean metrics of two segmentation masks, via Keras.
    
    IoU(A,B) = |A & B| / (| A U B|)
    Dice(A,B) = 2*|A & B| / (|A| + |B|)
    
    Args:
        y_true: true masks, one-hot encoded.
        y_pred: predicted masks, either softmax outputs, or one-hot encoded.
        metric_name: metric to be computed, either 'iou' or 'dice'.
        metric_type: one of 'standard' (default), 'soft', 'naive'.
          In the standard version, y_pred is one-hot encoded and the mean
          is taken only over classes that are present (in y_true or y_pred).
          The 'soft' version of the metrics are computed without one-hot 
          encoding y_pred.
          The 'naive' version return mean metrics where absent classes contribute
          to the class mean as 1.0 (instead of being dropped from the mean).
        drop_last = True: boolean flag to drop last class (usually reserved
          for background class in semantic segmentation)
        mean_per_class = False: return mean along batch axis for each class.
        verbose = False: print intermediate results such as intersection, union
          (as number of pixels).
    Returns:
        IoU/Dice of y_true and y_pred, as a float, unless mean_per_class == True
          in which case it returns the per-class metric, averaged over the batch.
    
    Inputs are B*W*H*N tensors, with
        B = batch size,
        W = width,
        H = height,
        N = number of classes
    """

    flag_soft = (metric_type == 'soft')
    flag_naive_mean = (metric_type == 'naive')
    
    # always assume one or more classes
    #num_classes = K.shape(y_true)[-1]
        
    if not flag_soft:
        # get one-hot encoded masks from y_pred (true masks should already be one-hot)
        y_pred = K.one_hot(K.argmax(y_pred), num_classes)
        y_true = K.one_hot(K.argmax(y_true), num_classes)

    # if already one-hot, could have skipped above command
    # keras uses float32 instead of float64, would give error down (but numpy arrays or keras.to_categorical gives float64)
    y_true = K.cast(y_true, 'float32')
    y_pred = K.cast(y_pred, 'float32')

    # intersection and union shapes are batch_size * n_classes (values = area in pixels)
    axes = (1,2) # W,H axes of each image
    intersection = K.sum(K.abs(y_true * y_pred), axis=axes)
    mask_sum = K.sum(K.abs(y_true), axis=axes) + K.sum(K.abs(y_pred), axis=axes)
    union = mask_sum  - intersection # or, np.logical_or(y_pred, y_true) for one-hot

    smooth = .001
    iou = (intersection + smooth) / (union + smooth)
    dice = 2 * (intersection + smooth)/(mask_sum + smooth)

    metric = {'iou': iou, 'dice': dice}[metric_name]

    # define mask to be 0 when no pixels are present in either y_true or y_pred, 1 otherwise
    mask =  K.cast(K.not_equal(union, 0), 'float32')
    
    if drop_last:
        metric = metric[:,:-1]
        mask = mask[:,:-1]
    
    if verbose:
        print('intersection, union')
        print(K.eval(intersection), K.eval(union))
        print(K.eval(intersection/union))
    
    # return mean metrics: remaining axes are (batch, classes)
    if flag_naive_mean:
        return K.mean(metric)

    # take mean only over non-absent classes
    class_count = K.sum(mask, axis=0)
    non_zero = tf.greater(class_count, 0)
    non_zero_sum = tf.boolean_mask(K.sum(metric * mask, axis=0), non_zero)
    non_zero_count = tf.boolean_mask(class_count, non_zero)
    
    if verbose:
        print('Counts of inputs with class present, metrics for non-absent classes')
        print(K.eval(class_count), K.eval(non_zero_sum / non_zero_count))
        
    return K.mean(non_zero_sum / non_zero_count)

def mean_iou(y_true, y_pred, **kwargs):
    """
    Compute mean Intersection over Union of two segmentation masks, via Keras.
    Calls metrics_k(y_true, y_pred, metric_name='iou'), see there for allowed kwargs.
    """
    return seg_metrics(y_true, y_pred, metric_name='iou', **kwargs)

def mean_dice(y_true, y_pred, **kwargs):
    """
    Compute mean Dice coefficient of two segmentation masks, via Keras.
    Calls metrics_k(y_true, y_pred, metric_name='iou'), see there for allowed kwargs.
    """
    return seg_metrics(y_true, y_pred, metric_name='dice', **kwargs)

#https://stackoverflow.com/questions/51793737/custom-loss-function-for-u-net-in-keras-using-class-weights-class-weight-not
def weightedLoss(originalLossFunc, weightsList):

    def lossFunc(true, pred):

        axis = -1 #if channels last 
        #axis=  1 #if channels first

        #argmax returns the index of the element with the greatest value
        #done in the class axis, it returns the class index
        classSelectors = K.argmax(true, axis=axis) 
        #if your loss is sparse, use only true as classSelectors

        #considering weights are ordered by class, for each class
        #true(1) if the class index is equal to the weight index   
        classSelectors = [K.equal(tf.cast(i, tf.int32), tf.cast(classSelectors, tf.int32)) for i in range(len(weightsList))]

        #casting boolean to float for calculations  
        #each tensor in the list contains 1 where ground true class is equal to its index 
        #if you sum all these, you will get a tensor full of ones. 
        classSelectors = [K.cast(x, K.floatx()) for x in classSelectors]

        #for each of the selections above, multiply their respective weight
        weights = [sel * w for sel,w in zip(classSelectors, weightsList)] 

        #sums all the selections
        #result is a tensor with the respective weight for each element in predictions
        weightMultiplier = weights[0]
        for i in range(1, len(weights)):
            weightMultiplier = weightMultiplier + weights[i]


        #make sure your originalLossFunc only collapses the class axis
        #you need the other axes intact to multiply the weights tensor
        loss = originalLossFunc(true,pred) 
        loss = loss * weightMultiplier

        return loss
    return lossFunc


class CheckpointsCallback(Callback):
    def __init__(self, checkpoints_path):
        self.checkpoints_path = checkpoints_path

    def on_epoch_end(self, epoch, logs=None):
        if self.checkpoints_path is not None:
            self.model.save_weights(self.checkpoints_path + "." + str(epoch))
            print("saved ", self.checkpoints_path + "." + str(epoch))


def train(model,
          train_images,
          train_annotations,
          input_height=None,
          input_width=None,
          n_classes=None,
          verify_dataset=True,
          checkpoints_path=None,
          csv_log_path='log.csv',
          epochs=5,
          batch_size=2,
          validate=False,
          val_images=None,
          val_annotations=None,
          val_batch_size=2,
          auto_resume_checkpoint=False,
          load_weights=None,
          steps_per_epoch=512,
          val_steps_per_epoch=512,
          gen_use_multiprocessing=False,
          ignore_zero_class=False,
          optimizer_name='adadelta',
          do_augment=False,
          augmentation_name="aug_all",
          metric_name='accuracy',
          patience=15,
          monitor='val_loss',
          class_weights=None,
          custom_loss=None):

    from .models.all_models import model_from_name
    # check if user gives model name instead of the model object
    if isinstance(model, six.string_types):
        # create the model from the name
        assert (n_classes is not None), "Please provide the n_classes"
        if (input_height is not None) and (input_width is not None):
            model = model_from_name[model](
                n_classes, input_height=input_height, input_width=input_width)
        else:
            model = model_from_name[model](n_classes)

    n_classes = model.n_classes
    input_height = model.input_height
    input_width = model.input_width
    output_height = model.output_height
    output_width = model.output_width

    if validate:
        assert val_images is not None
        assert val_annotations is not None

    if optimizer_name is not None:

        if ignore_zero_class:
            loss_k = masked_categorical_crossentropy
        else:
            loss_k = 'categorical_crossentropy'

        if metric_name == 'iou':
            metrics = [MeanIoU(num_classes=n_classes)]
        elif metric_name == "accuracy_and_iou":
            metrics = [MeanIoU(num_classes=n_classes), 'accuracy']
        elif metric_name == "dice":
            metrics = [mean_dice, 'accuracy']
        else:
            metrics=['accuracy']

        if optimizer_name == 'adam':
            optimizer = Adam(lr=0.0001, beta_1=0.9)
        else:
            optimizer = optimizer_name

        if custom_loss:
            if custom_loss == 'dice_loss':
                loss_k = dice_coef_multilabel
            else:
                loss_k = jaccard_distance

        print('loss_name: ', loss_k)
        print('weightedLoss: ', class_weights)
        print('optimizer_name: ', optimizer)
        print('metrics: ', metrics)
        print('monitor: ', monitor)
        print('do_augment: ', do_augment)
        print('augmentation_name: ', augmentation_name)

        if class_weights:
            loss_k = weightedLoss(loss_k, class_weights)

        model.compile(loss=loss_k,
                      optimizer=optimizer,
                      metrics=metrics)

    if checkpoints_path is not None:
        with open(checkpoints_path+"_config.json", "w") as f:
            json.dump({
                "model_class": model.model_name,
                "n_classes": n_classes,
                "input_height": input_height,
                "input_width": input_width,
                "output_height": output_height,
                "output_width": output_width
            }, f)

    if load_weights is not None and len(load_weights) > 0:
        print("Loading weights from ", load_weights)
        model.load_weights(load_weights)

    if auto_resume_checkpoint and (checkpoints_path is not None):
        #latest_checkpoint = find_latest_checkpoint(checkpoints_path)
        latest_checkpoint = find_latest_checkpoint_by_date(checkpoints_path)
        if latest_checkpoint is not None:
            print("Loading the weights from latest checkpoint ",
                  latest_checkpoint)
            model.load_weights(latest_checkpoint)

    if verify_dataset:
        print("Verifying training dataset")
        verified = verify_segmentation_dataset(train_images,
                                               train_annotations,
                                               n_classes)
        assert verified
        if validate:
            print("Verifying validation dataset")
            verified = verify_segmentation_dataset(val_images,
                                                   val_annotations,
                                                   n_classes)
            assert verified

    train_gen = image_segmentation_generator(
        train_images, train_annotations,  batch_size,  n_classes,
        input_height, input_width, output_height, output_width,
        do_augment=do_augment, augmentation_name=augmentation_name)

    if validate:
        val_gen = image_segmentation_generator(
            val_images, val_annotations,  val_batch_size,
            n_classes, input_height, input_width, output_height, output_width)

    if 'val_mean_io_u' in monitor or 'val_mean_dice' in monitor:
        #monitor = 'val_mean_io_u'
        mode = 'max'
    else:
        #monitor = 'val_loss'
        mode = 'min'

    earlystopping_callback = EarlyStopping(monitor=monitor, patience=patience, mode=mode)
    #filepath = checkpoints_file_path if checkpoints_file_path else checkpoints_path
    filepath = checkpoints_path
    checkpoint_callback = ModelCheckpoint(filepath = filepath, save_best_only=True, monitor=monitor, save_weights_only=True)
    csvlog_callback=CSVLogger(filename=csv_log_path)

    reduce_lr = ReduceLROnPlateau(monitor=monitor, factor=0.5, patience=40, min_lr=0.000025)

    #callbacks = [
    #    CheckpointsCallback(checkpoints_path)
    #]

    callbacks = [
        earlystopping_callback, checkpoint_callback, csvlog_callback
    ]

    if optimizer_name == 'adam':
        print('include callback reduce_lr')
        callbacks.append(reduce_lr)

    history = None

    if not validate:
        history = model.fit_generator(train_gen, steps_per_epoch,
                            epochs=epochs, callbacks=callbacks)
    else:
        history = model.fit_generator(train_gen,
                            steps_per_epoch,
                            validation_data=val_gen,
                            validation_steps=val_steps_per_epoch,
                            epochs=epochs, callbacks=callbacks,
                            use_multiprocessing=gen_use_multiprocessing)

    return history
