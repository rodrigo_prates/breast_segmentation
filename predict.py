from keras_segmentation.models.unet import vgg_unet, resnet50_unet, mobilenet_unet
from keras_segmentation.models.config import IMAGE_ORDERING
from keras_segmentation.data_utils.data_loader import get_image_array
import argparse
import os
import numpy as np
import cv2


SEGMODELS = {
      "vgg_unet": vgg_unet,
      "resnet50_unet": resnet50_unet,
      "mobilenet_unet": mobilenet_unet
    }

TRAGETSIZE = {
      "vgg_unet": (224, 224),
      "resnet50_unet": (224, 224),
      "mobilenet_unet": (224, 224)
   }

LABELS = {(0, 64, 0): 0, 
          (82, 82, 146): 1, 
          (212, 212, 148): 2}

def _main_(args):
    modelname = args.modelname
    modelpath = args.model
    image_path = args.image
    position = args.position
    output_path = args.output

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    Network = SEGMODELS[modelname]

    input_height, input_width = TRAGETSIZE[modelname]

    model = Network(n_classes=3, input_height=input_height, input_width=input_width)

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])

    model.load_weights(modelpath)

    output_width = model.output_width
    output_height  = model.output_height

    img = cv2.imread(image_path)
    height, width = img.shape[:2]

    flipped = False
    img, flipped = flip_image(img, position)

    imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)
    pr = model.predict(np.array([imgwindow_arr]))[0]
    pr = pr.reshape((output_height, output_width, 3)).argmax( axis=2 )

    seg_img = np.zeros((output_height, output_width, 3))

    for c in list(LABELS.values()):
        color = list(LABELS.keys())[list(LABELS.values()).index(c)]
        seg_img[:,:,0] += ( (pr[:,: ] == c )*( color[2] )).astype('uint8')
        seg_img[:,:,1] += ((pr[:,: ] == c )*( color[1] )).astype('uint8')
        seg_img[:,:,2] += ((pr[:,: ] == c )*( color[0] )).astype('uint8')

    if flipped:
        seg_img = cv2.flip(seg_img, 1)
        
    seg_img = cv2.resize(seg_img, (width, height), interpolation=cv2.INTER_NEAREST)

    cv2.imwrite(os.path.sep.join([ output_path, 'predseg_' + os.path.basename(image_path) ]), seg_img)

def flip_image(image, new_pos='left'):
    flipped = False
    image_pos = check_image_orientation(image)
    if image_pos != new_pos:
        image = cv2.flip(image, 1)
        flipped = True
    return image, flipped

def check_image_orientation(image):
    img_orientation = None
    if isinstance(image, str):
        img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    else:
        img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img_sum = np.sum(img, axis=0)
    img_mean = int(len(img_sum)/2)
    left_sum = np.sum(img_sum[0:img_mean])
    right_sum = np.sum(img_sum[img_mean::])
    if left_sum > right_sum:
        img_orientation = 'left'
    else:
        img_orientation = 'right'
    return img_orientation


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Breast Segmentation Prediction Script')
    argparser.add_argument('-n', '--modelname', help='model name')
    argparser.add_argument('-m', '--model', help='path to model')
    argparser.add_argument('-i', '--image', help='path to an image')
    argparser.add_argument('-p', '--position', default='left', help='image orientation: left or right')    
    argparser.add_argument('-o', '--output', default='output/', help='path to output directory')   
    
    args = argparser.parse_args()
    _main_(args)