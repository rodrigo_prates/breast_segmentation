# CNN for breast mammography segmentation #

Here, three CNN-based semantic segmentation models for mammography segmentation are provided from three datasets (MIAS, INbreast and MIAS+INbreast):

1. Unet with Mobilenetv1 Encoder
2. Unet with Resnet50 Encoder
3. Unet with VGG16 Encoder

## INSTALLATION ##

Requirements:

- python 3+
- keras_segmentation
- OpenCV
  
keras_segmentation is a modified fork from https://github.com/divamgupta/image-segmentation-keras

- cd keras_segmentation
- python setup.py install

## EXAMPLE ##
python predict.py -n mobilenet_unet -m mobilenet_unet/mobilenet_unet_mias -i images/mdb001.png

Example results for the pre-trained models provided :

Input Image            |  Output Segmentation Image
:-------------------------:|:-------------------------:
![mdb001](mdb001.png) | ![mdb001](predseg_mdb001.png)